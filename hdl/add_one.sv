`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.05.2018 21:07:27
// Design Name: 
// Module Name: add_one
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module add_one(
    input           clk,
    input   [31:0]  inArg,
    output  [31:0]  outArg
);

    reg     [7:0]   result0;
    reg     [7:0]   result1;
    reg     [7:0]   result2;
    reg     [7:0]   result3;
    assign  outArg[7:0]     =   result0;
    assign  outArg[15:8]    =   result1;
    assign  outArg[23:16]   =   result2;
    assign  outArg[31:24]   =   result3;

    always_ff @(posedge clk) begin
    
        result0 = inArg[7:0]    +   1'b1;
        result1 = inArg[15:8]   +   1'b1;
        result2 = inArg[23:16]  +   1'b1;
        result3 = inArg[31:24]  +   1'b1;
    
    end

endmodule
