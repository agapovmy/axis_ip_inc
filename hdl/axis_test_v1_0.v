
`timescale 1 ns / 1 ps

	module axis_test_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXIS
		parameter integer C_S00_AXIS_TDATA_WIDTH	= 32,

		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32
	)
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line

		// Ports of Axi Slave Bus Interface S00_AXIS
		input wire  s00_axis_aclk,
		input wire  s00_axis_aresetn,
		output wire  s00_axis_tready,
		input wire [C_S00_AXIS_TDATA_WIDTH-1 : 0] s00_axis_tdata,
		input wire [(C_S00_AXIS_TDATA_WIDTH/8)-1 : 0] s00_axis_tstrb,
		input wire  s00_axis_tlast,
		input wire  s00_axis_tvalid,

		// Ports of Axi Master Bus Interface M00_AXIS
		input wire  m00_axis_aclk,
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready
	);
	
// Instantiation of Axi Bus Interface S00_AXIS
	axis_test_v1_0_S00_AXIS # ( 
		.C_S_AXIS_TDATA_WIDTH(C_S00_AXIS_TDATA_WIDTH)
	) axis_test_v1_0_S00_AXIS_inst (
//	    .processedData(procWireM),                    //Тут надо как-то хитровыебанно передать m00_axis_tdata, но я не знаю как
		.S_AXIS_ACLK(s00_axis_aclk),
		.S_AXIS_ARESETN(s00_axis_aresetn),
		.S_AXIS_TREADY(s00_axis_tready),
		.S_AXIS_TDATA(s00_axis_tdata),
		.S_AXIS_TSTRB(s00_axis_tstrb),
		.S_AXIS_TLAST(s00_axis_tlast),
		.S_AXIS_TVALID(s00_axis_tvalid)
	);

    wire[C_M00_AXIS_TDATA_WIDTH-1:0]    tdataW;

// Instantiation of Axi Bus Interface M00_AXIS
	axis_test_v1_0_M00_AXIS # ( 
		.C_M_AXIS_TDATA_WIDTH(C_M00_AXIS_TDATA_WIDTH),
		.C_M_START_COUNT(C_M00_AXIS_START_COUNT)
	) axis_test_v1_0_M00_AXIS_inst (
	    .procData(tdataW),
		.M_AXIS_ACLK(m00_axis_aclk),
		.M_AXIS_ARESETN(m00_axis_aresetn),
		.M_AXIS_TVALID(m00_axis_tvalid),
		.M_AXIS_TDATA(m00_axis_tdata),
		.M_AXIS_TSTRB(m00_axis_tstrb),
		.M_AXIS_TLAST(m00_axis_tlast),
		.M_AXIS_TREADY(m00_axis_tready)
	);

	// Add user logic here

//    assign m00_axis_aclk    =   s00_axis_aclk;
//    assign m00_axis_aresetn =   s00_axis_aresetn;
    
    reg [C_M00_AXIS_TDATA_WIDTH-1:0]    tdata;
//    reg [C_M00_AXIS_TDATA_WIDTH/8-1:0]  tstrb;
//    reg tvalid;
//    reg tlast;
//    reg tready;
    
    assign tdataW           =   tdata;
//    assign m00_axis_tstrb   =   tstrb;
//    assign m00_axis_tvalid  =   tvalid;
//    assign m00_axis_tlast   =   tlast;
//    assign m00_axis_tready  =   tready;
    
    always @(posedge m00_axis_aclk) begin
        tdata[7:0] <= s00_axis_tdata[7:0]     + 1'b1;
        tdata[15:8] <= s00_axis_tdata[15:8]   + 1'b1;
        tdata[23:16] <= s00_axis_tdata[23:16] + 1'b1;
        tdata[31:24] <= s00_axis_tdata[31:24] + 1'b1;
        
//        tstrb <= s00_axis_tstrb;
//        tvalid <= s00_axis_tvalid;
//        tlast <= s00_axis_tlast;
//        tready <= s00_axis_tready;
    end

	// User logic ends

	endmodule
